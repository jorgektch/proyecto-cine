-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 03-07-2021 a las 06:28:12
-- Versión del servidor: 8.0.22
-- Versión de PHP: 8.0.0

START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `tienda`
--
CREATE DATABASE IF NOT EXISTS `tienda` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci;
USE tienda;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categoria`
--

CREATE TABLE `categoria` (
  `id_catP1` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_spanish2_ci NOT NULL,
  `nom_cat` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_spanish2_ci NOT NULL,
  `descr_cat` text CHARACTER SET utf8mb4 COLLATE utf8mb4_spanish2_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish2_ci;

--
-- Volcado de datos para la tabla `categoria`
--

INSERT INTO `categoria` (`id_catP1`, `nom_cat`, `descr_cat`) VALUES
('100', 'Productos lácteos', 'Todos los tipos de leche, que la empresa produce'),
('101', 'Derivados de la leche', 'Los productos derivados de la leche producidos por la empresa');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categoria cliente`
--

CREATE TABLE `categoria cliente` (
  `id_catC` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_spanish2_ci NOT NULL,
  `descripcion` text CHARACTER SET utf8mb4 COLLATE utf8mb4_spanish2_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish2_ci;

--
-- Volcado de datos para la tabla `categoria cliente`
--

INSERT INTO `categoria cliente` (`id_catC`, `descripcion`) VALUES
('1', 'Cliente-compra por menor'),
('2', 'Cliente-compra por mayor');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cliente`
--

CREATE TABLE `cliente` (
  `id_cliente` int NOT NULL,
  `nom_cliente` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_spanish2_ci NOT NULL,
  `apeM_cliente` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_spanish2_ci NOT NULL,
  `apeP_cliente` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_spanish2_ci NOT NULL,
  `sexo_cliente` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_spanish2_ci NOT NULL,
  `Fnaci_cliente` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_spanish2_ci NOT NULL,
  `cel_cliente` varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_spanish2_ci NOT NULL,
  `correo_cliente` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_spanish2_ci NOT NULL,
  `id_direcc1` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_spanish2_ci DEFAULT NULL,
  `id_catC1` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_spanish2_ci DEFAULT NULL,
  `clave_cliente` varchar(45) COLLATE utf8mb4_spanish2_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish2_ci;

--
-- Volcado de datos para la tabla `cliente`
--

INSERT INTO `cliente` (`id_cliente`, `nom_cliente`, `apeM_cliente`, `apeP_cliente`, `sexo_cliente`, `Fnaci_cliente`, `cel_cliente`, `correo_cliente`, `id_direcc1`, `id_catC1`, `clave_cliente`) VALUES
(27364859, 'Sthephany Valeria', 'Valdivia', 'Valdivia', 'M', '24/08/1976', '937456789', 'Sthe@gmail.com', 'Alameda Mza. 25 Lote', '1', '123'),
(87654567, 'Fabian Esteban', 'Rojas', 'Quispe', 'H', '23/06/1985', '987123654', 'Fabian.Esteb@gmail.com', 'Tomilla Mz.6 Lt 20', '1', NULL),
(87689701, 'Manuli Sergio', 'Ramirez', 'Rojas', 'H', '05/09/2000', '987345123', 'mandil.r@gmail.com', 'Cayma Urb. San Antonio', '1', NULL),
(89765432, 'Melanie Valeria', 'Valdivia', 'Merma', 'M', '08/12/1990', '987654456', 'melu.vale@gmail.com', 'Cerro Colorado Mz.4 Lt19', '1', NULL),
(92736478, 'Diego Gonzalo', 'Gonzalo', 'Pineda', 'H', '03/09/1990', '987345678', 'Diego@gmail.com', ' Rodríguez 102  Miraflores', '1', NULL),
(94758697, 'Fabian Sergio', 'Romero', 'Castillo', 'H', '12/12/2000', '987467586', 'Fabi@gmail.com', 'Urb mariscal Castilla', '2', NULL),
(98346758, 'Jaime Bayly', 'Bichincha', 'Palacios', 'H', '05/08/1975', '983475678', 'bayly.jaime@gmail.com', 'Alfonso Ugarte Mz.4 Lt9', '2', NULL),
(98645609, 'Sofia Valeria', 'Quispe', 'Mamani', 'M', '24/09/1994', '987456765', 'm.sofis@gmail.com', 'Urb.Los Guindos Cayma Lt.4', '1', NULL),
(98734567, 'Pedro Sergio', 'Velasco', 'Torres', 'H', '13/06/1976', '987345645', 'Vels.pedro@gmail.com', 'Urbanizacion La Molina', '2', NULL),
(98745676, 'Dessiré Abigail', 'Agreda', 'Acevedo', 'M', '15/10/2000', '987456190', 'Dessi.Agreda@gmail.com', 'Yanahuara Mz. 4 Misti ', '1', NULL),
(98745677, 'Jorge Jesús', 'solis', 'Quispe Villaverde', 'F', '2021-06-03', '888888', 'Cesar@gmail.com', NULL, NULL, NULL),
(98745678, 'Pepito', 'Solis', 'Solis', 'M', '2021-06-03', '888888', 'pepito@gmail.com', NULL, NULL, '123'),
(98745679, 'Jorge', 'Solis', 'Ruiz', 'M', '2021-07-03', '555555', 'jorge@gmail.com', NULL, NULL, '111111');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `compra`
--

CREATE TABLE `compra` (
  `id_comp` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_spanish2_ci NOT NULL,
  `fecha_comp` date NOT NULL,
  `subT_compra` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_spanish2_ci NOT NULL,
  `total_compra` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_spanish2_ci NOT NULL,
  `id_provee1` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_spanish2_ci NOT NULL,
  `cant_prodC` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_spanish2_ci NOT NULL,
  `id_prod1` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_spanish2_ci NOT NULL,
  `Precio_unitario` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_spanish2_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish2_ci;

--
-- Volcado de datos para la tabla `compra`
--

INSERT INTO `compra` (`id_comp`, `fecha_comp`, `subT_compra`, `total_compra`, `id_provee1`, `cant_prodC`, `id_prod1`, `Precio_unitario`) VALUES
('C001', '2021-05-01', 'S/1705.00', 'S/2011.90', '1', '550', '002', 'S/3.10'),
('C002', '2021-05-04', 'S/1240.00', 'S/1463.20', '1', '400', '002', 'S/3.10'),
('C003', '2021-04-14', 'S/2100', 'S/2478.00', '1', '600', '003', 'S/3.50'),
('C004', '2021-05-05', 'S/4585.00', 'S/5410.30', '1', '350', '004', 'S/13.10'),
('C005', '2021-05-01', 'S/4500.00', 'S/5310.00', '1', '250', '001', 'S/18.00'),
('C006', '2020-12-16', 'S/5360.00', 'S/6324.80', '1', '670', '006', 'S/8.00'),
('C007', '2021-05-12', 'S/8100.00', 'S/9558.00', '1', '450', '001', 'S/18.00'),
('C008', '2021-06-02', 'S/7912.40', 'S/9336.63', '1', '604', '004', 'S/13.10'),
('C009', '2021-06-04', 'S/2500.00', 'S/2950.00', '1', '500', '005', 'S/5.00'),
('C010', '2021-06-05', 'S/1240.00', 'S/1463.20', '1', '400', '002', 'S/3.10');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `direccion`
--

CREATE TABLE `direccion` (
  `id_direccion` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_spanish2_ci NOT NULL,
  `distrito` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_spanish2_ci NOT NULL,
  `calle` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_spanish2_ci NOT NULL,
  `num_casa` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_spanish2_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish2_ci;

--
-- Volcado de datos para la tabla `direccion`
--

INSERT INTO `direccion` (`id_direccion`, `distrito`, `calle`, `num_casa`) VALUES
(' Rodríguez 102  Miraflores', 'Arequipa', 'Rodriguez', '2'),
('Alameda Mza. 25 Lote', 'Cerro Colorado', 'Alameda', '3'),
('Alfonso Ugarte Mz.4 Lt9', 'Tiabaya', 'Calle Ugarte', '34'),
('Cayma Urb. San Antonio', 'Cayma', 'San Antonio', '10'),
('Cerro Colorado Mz.4 Lt19', 'Cerro Colorado', 'Calle urubamba', '9'),
('Tomilla Mz.6 Lt 20', 'Cayma', 'Av. Bolognesi', '6'),
('Urb mariscal Castilla', 'Cerro Colorado', 'calle mariscal', '2'),
('Urb.Los Guindos Cayma Lt.4', 'Cayma', 'Av. Cayma', '4'),
('Urbanizacion La Molina', 'Arequipa', 'La molina', '3'),
('Yanahuara Mz. 4 Misti ', 'Yanahuara', 'Calle Misti', '5');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `producto`
--

CREATE TABLE `producto` (
  `id_pro` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_spanish2_ci NOT NULL,
  `nom_pro` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_spanish2_ci NOT NULL,
  `stock_prod` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_spanish2_ci NOT NULL,
  `foto_prod` text CHARACTER SET utf8mb4 COLLATE utf8mb4_spanish2_ci NOT NULL,
  `id_catP` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_spanish2_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish2_ci;

--
-- Volcado de datos para la tabla `producto`
--

INSERT INTO `producto` (`id_pro`, `nom_pro`, `stock_prod`, `foto_prod`, `id_catP`) VALUES
('001', 'queso', '50', 'queso.jpg', '101'),
('002', 'leche deslactosada', '50', 'leche.jpg', '100'),
('003', 'leche en polvo', '50', 'leche-polvo.jpg', '100'),
('004', 'leche entera', '50', 'leche-entera.webp', '100'),
('005', 'yogurt', '50', 'yougurt.webp', '101'),
('006', 'mantequilla', '50', 'mantequilla.jpg', '101');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `prod_venta`
--

CREATE TABLE `prod_venta` (
  `id_venta1` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_spanish2_ci NOT NULL,
  `id_prod2` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_spanish2_ci NOT NULL,
  `cant_prodV` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_spanish2_ci NOT NULL,
  `precio_prodV` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_spanish2_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish2_ci;

--
-- Volcado de datos para la tabla `prod_venta`
--

INSERT INTO `prod_venta` (`id_venta1`, `id_prod2`, `cant_prodV`, `precio_prodV`) VALUES
('00001', '001', '20', '4'),
('00002', '002', '25', '4'),
('00003', '003', '12', '6'),
('00004', '004', '10', '7'),
('00005', '005', '8', '4'),
('00006', '006', '30', '8'),
('00007', '006', '8', '8'),
('00008', '005', '10', '4'),
('00009', '004', '20', '7'),
('00010', '003', '3', '6');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proveedor`
--

CREATE TABLE `proveedor` (
  `id_provee` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_spanish2_ci NOT NULL,
  `nom_provee` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_spanish2_ci NOT NULL,
  `RUC` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_spanish2_ci NOT NULL,
  `direc_provee` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_spanish2_ci NOT NULL,
  `telf_provee` varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_spanish2_ci NOT NULL,
  `correo_provee` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_spanish2_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish2_ci;

--
-- Volcado de datos para la tabla `proveedor`
--

INSERT INTO `proveedor` (`id_provee`, `nom_provee`, `RUC`, `direc_provee`, `telf_provee`, `correo_provee`) VALUES
('1', 'VAQUITA', '1098765430', 'MAJES', '987654321', 'vaquita@gmail.com');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `vendedor`
--

CREATE TABLE `vendedor` (
  `id_vende` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_spanish2_ci NOT NULL,
  `DNI_vende` varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_spanish2_ci NOT NULL,
  `nombre_vende` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_spanish2_ci NOT NULL,
  `ape_vende` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_spanish2_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish2_ci;

--
-- Volcado de datos para la tabla `vendedor`
--

INSERT INTO `vendedor` (`id_vende`, `DNI_vende`, `nombre_vende`, `ape_vende`) VALUES
('01', '12345678', 'Carlos', 'Carbajal'),
('02', '23456789', 'María', 'Cabrera'),
('03', '34567890', 'Karla', 'Fernandez');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `venta`
--

CREATE TABLE `venta` (
  `id_venta` int NOT NULL,
  `id_cliente` int NOT NULL,
  `monto_venta` varchar(45) NOT NULL,
  `cantidad_venta` varchar(45) NOT NULL,
  `fecha_venta` varchar(45) NOT NULL,
  `nombre_producto` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Volcado de datos para la tabla `venta`
--

INSERT INTO `venta` (`id_venta`, `id_cliente`, `monto_venta`, `cantidad_venta`, `fecha_venta`, `nombre_producto`) VALUES
(1, 27364859, '4', '1', '2021-07-01 00:29:50', NULL),
(2, 27364859, '4', '1', '2021-07-01 00:31:30', NULL),
(3, 27364859, '4', '1', '2021-07-01 00:31:44', NULL),
(4, 27364859, '8', '1', '2021-07-01 00:31:53', NULL),
(5, 27364859, '4', '1', '2021-07-01 00:37:08', 'queso'),
(6, 27364859, '4', '1', '2021-07-01 00:37:13', 'leche deslactosada'),
(7, 27364859, '4', '1', '2021-07-01 00:55:43', 'yogurt'),
(8, 27364859, '4', '1', '2021-07-01 00:55:54', 'queso'),
(9, 27364859, '4', '1', '2021-07-01 00:57:09', 'leche deslactosada'),
(10, 27364859, '4', '1', '2021-07-01 00:57:41', 'queso'),
(11, 27364859, '6', '1', '2021-07-01 00:57:56', 'leche en polvo'),
(12, 27364859, '6', '1', '2021-07-01 10:59:54', 'leche en polvo'),
(13, 27364859, '4', '1', '2021-07-01 11:00:39', 'yogurt'),
(14, 27364859, '8', '1', '2021-07-01 11:21:49', 'mantequilla'),
(15, 27364859, '4', '1', '2021-07-01 11:22:00', 'yogurt'),
(16, 27364859, '4', '1', '2021-07-01 11:25:49', 'leche deslactosada'),
(17, 27364859, '4', '1', '2021-07-01 11:31:27', 'queso'),
(18, 27364859, '4', '1', '2021-07-01 11:34:37', 'queso'),
(19, 27364859, '4', '1', '2021-07-01 11:34:56', 'leche deslactosada');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ventas`
--

CREATE TABLE `ventas` (
  `id_ventas` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_spanish2_ci NOT NULL,
  `id_cliente1` int NOT NULL,
  `id_vende1` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_spanish2_ci DEFAULT NULL,
  `cliente_ventas` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_spanish2_ci DEFAULT NULL,
  `fech_venta` date NOT NULL,
  `subT_venta` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_spanish2_ci DEFAULT NULL,
  `total_venta` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_spanish2_ci NOT NULL,
  `cant_vent` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_spanish2_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish2_ci;

--
-- Volcado de datos para la tabla `ventas`
--

INSERT INTO `ventas` (`id_ventas`, `id_cliente1`, `id_vende1`, `cliente_ventas`, `fech_venta`, `subT_venta`, `total_venta`, `cant_vent`) VALUES
('00001', 87654567, '01', '', '2021-05-10', '65', '80', '20'),
('00002', 87689701, '01', '', '2021-04-15', '82', '100', '25'),
('00003', 89765432, '01', '', '2021-05-20', '59.04', '72', '12'),
('00004', 98346758, '02', '', '2021-05-25', '57.4', '70', '10'),
('00005', 98645609, '03', '', '2021-05-30', '26.24', '32', '8'),
('00006', 98745676, '03', '', '2021-06-03', '196.8', '240', '30'),
('00007', 27364859, '03', '', '2021-05-12', '52.48', '64', '8'),
('00008', 92736478, '02', '', '2021-05-18', '32.8', '40', '10'),
('00009', 94758697, '02', '', '2021-05-28', '114.8', '140', '20'),
('00010', 98734567, '02', '', '2021-06-06', '14.76', '18', '3');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `categoria`
--
ALTER TABLE `categoria`
  ADD PRIMARY KEY (`id_catP1`);

--
-- Indices de la tabla `categoria cliente`
--
ALTER TABLE `categoria cliente`
  ADD PRIMARY KEY (`id_catC`);

--
-- Indices de la tabla `cliente`
--
ALTER TABLE `cliente`
  ADD PRIMARY KEY (`id_cliente`),
  ADD KEY `id_direcc1` (`id_direcc1`,`id_catC1`),
  ADD KEY `id_catC1` (`id_catC1`);

--
-- Indices de la tabla `compra`
--
ALTER TABLE `compra`
  ADD PRIMARY KEY (`id_comp`),
  ADD KEY `id_prod1` (`id_prod1`),
  ADD KEY `id_provee1` (`id_provee1`);

--
-- Indices de la tabla `direccion`
--
ALTER TABLE `direccion`
  ADD PRIMARY KEY (`id_direccion`);

--
-- Indices de la tabla `producto`
--
ALTER TABLE `producto`
  ADD PRIMARY KEY (`id_pro`),
  ADD KEY `id_catP` (`id_catP`);

--
-- Indices de la tabla `prod_venta`
--
ALTER TABLE `prod_venta`
  ADD PRIMARY KEY (`id_venta1`,`id_prod2`),
  ADD KEY `id_venta1` (`id_venta1`,`id_prod2`),
  ADD KEY `id_prod2` (`id_prod2`);

--
-- Indices de la tabla `proveedor`
--
ALTER TABLE `proveedor`
  ADD PRIMARY KEY (`id_provee`);

--
-- Indices de la tabla `vendedor`
--
ALTER TABLE `vendedor`
  ADD PRIMARY KEY (`id_vende`);

--
-- Indices de la tabla `venta`
--
ALTER TABLE `venta`
  ADD PRIMARY KEY (`id_venta`);

--
-- Indices de la tabla `ventas`
--
ALTER TABLE `ventas`
  ADD PRIMARY KEY (`id_ventas`),
  ADD KEY `id_cliente1` (`id_cliente1`,`id_vende1`),
  ADD KEY `id_vende1` (`id_vende1`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `cliente`
--
ALTER TABLE `cliente`
  MODIFY `id_cliente` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=98745680;

--
-- AUTO_INCREMENT de la tabla `venta`
--
ALTER TABLE `venta`
  MODIFY `id_venta` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `cliente`
--
ALTER TABLE `cliente`
  ADD CONSTRAINT `cliente_ibfk_1` FOREIGN KEY (`id_direcc1`) REFERENCES `direccion` (`id_direccion`) ON UPDATE CASCADE,
  ADD CONSTRAINT `cliente_ibfk_2` FOREIGN KEY (`id_catC1`) REFERENCES `categoria cliente` (`id_catC`) ON UPDATE CASCADE;

--
-- Filtros para la tabla `compra`
--
ALTER TABLE `compra`
  ADD CONSTRAINT `compra_ibfk_1` FOREIGN KEY (`id_prod1`) REFERENCES `producto` (`id_pro`) ON UPDATE CASCADE,
  ADD CONSTRAINT `compra_ibfk_2` FOREIGN KEY (`id_provee1`) REFERENCES `proveedor` (`id_provee`) ON UPDATE CASCADE;

--
-- Filtros para la tabla `producto`
--
ALTER TABLE `producto`
  ADD CONSTRAINT `producto_ibfk_1` FOREIGN KEY (`id_catP`) REFERENCES `categoria` (`id_catP1`) ON UPDATE CASCADE;

--
-- Filtros para la tabla `prod_venta`
--
ALTER TABLE `prod_venta`
  ADD CONSTRAINT `prod_venta_ibfk_1` FOREIGN KEY (`id_venta1`) REFERENCES `ventas` (`id_ventas`) ON UPDATE CASCADE,
  ADD CONSTRAINT `prod_venta_ibfk_2` FOREIGN KEY (`id_prod2`) REFERENCES `producto` (`id_pro`) ON UPDATE CASCADE;

--
-- Filtros para la tabla `ventas`
--
ALTER TABLE `ventas`
  ADD CONSTRAINT `ventas_ibfk_1` FOREIGN KEY (`id_vende1`) REFERENCES `vendedor` (`id_vende`) ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
