-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 09-06-2021 a las 08:25:47
-- Versión del servidor: 10.4.13-MariaDB
-- Versión de PHP: 7.2.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `cine`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `butaca`
--

CREATE TABLE `butaca` (
  `cod_but` varchar(4) NOT NULL,
  `num_but` varchar(4) NOT NULL,
  `fil_but` varchar(4) NOT NULL,
  `id_sala` varchar(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `butaca`
--

INSERT INTO `butaca` (`cod_but`, `num_but`, `fil_but`, `id_sala`) VALUES
('001', '01', 'A', '001'),
('002', '02', 'B', '002'),
('003', '13', 'A', '004'),
('004', '04', 'B', '003'),
('005', '05', 'B', '003');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cine`
--

CREATE TABLE `cine` (
  `id_cine` varchar(4) NOT NULL,
  `ubicacion_cine` varchar(50) NOT NULL,
  `telf_cine` int(9) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `cine`
--

INSERT INTO `cine` (`id_cine`, `ubicacion_cine`, `telf_cine`) VALUES
('001', 'LAMBRAMANI', 54908988),
('002', 'PORONGOCHE', 54678686);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cliente`
--

CREATE TABLE `cliente` (
  `dni_cliente` int(8) NOT NULL,
  `id_datos` varchar(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `cliente`
--

INSERT INTO `cliente` (`dni_cliente`, `id_datos`) VALUES
(72253827, '001'),
(70736765, '002'),
(73456378, '003'),
(30764583, '004'),
(464457899, '005'),
(70900031, '006'),
(31325644, '007');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `encargado_vent`
--

CREATE TABLE `encargado_vent` (
  `id_encargado` varchar(4) NOT NULL,
  `id_datos` varchar(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `encargado_vent`
--

INSERT INTO `encargado_vent` (`id_encargado`, `id_datos`) VALUES
('003', '008'),
('002', '009'),
('001', '010'),
('004', '011'),
('005', '012'),
('006', '013'),
('007', '014'),
('008', '015');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pelicula`
--

CREATE TABLE `pelicula` (
  `id_peli` varchar(4) NOT NULL,
  `gen_peli` varchar(15) NOT NULL,
  `nom_peli` varchar(30) NOT NULL,
  `clasi_peli` varchar(20) NOT NULL,
  `duracion_peli` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `pelicula`
--

INSERT INTO `pelicula` (`id_peli`, `gen_peli`, `nom_peli`, `clasi_peli`, `duracion_peli`) VALUES
('001', 'ACCION', 'DEPREDADOR', 'T', '90min'),
('002', 'INFANTIL', 'MADAGASCAR', 'E', '95min'),
('003', 'ROMANTICA', 'ANTES DE TI', 'T', '90min'),
('004', 'COMEDIA', 'LUNA DE MIEL', 'E', '95min'),
('005', 'TERROR', 'ANABELL', 'M', '90min'),
('006', 'INFANTIL', 'TOM Y JERY', 'E', '90min');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `persona`
--

CREATE TABLE `persona` (
  `id_datos` varchar(4) NOT NULL,
  `nom_datos` varchar(20) NOT NULL,
  `apelli_datos` varchar(30) NOT NULL,
  `telf_datos` int(9) NOT NULL,
  `correo_datos` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `persona`
--

INSERT INTO `persona` (`id_datos`, `nom_datos`, `apelli_datos`, `telf_datos`, `correo_datos`) VALUES
('001', 'MILUSKA', 'QUISPE', 950388341, 'miluska.quispe@gmail.com'),
('002', 'MICHAEL', 'MAMANI', 943156233, 'michael.123@gmail,com'),
('003', 'BEROLAY', 'PEREZ', 959567671, 'bero.perez@gmail.com'),
('004', 'ROCIO', 'MURILLO', 909056583, 'rocio.murillo@gmail.com'),
('005', 'JOSE', 'QUIROZ', 987899123, 'jose.quiroz@gmail.com'),
('006', 'FERNANDA', 'CHAVEZ', 959563444, 'fer.chavez@gmail.com'),
('007', 'ANA', 'LOAYZA', 938764532, 'ana.loayza@gmail.com'),
('008', 'PATRICIA', 'LOPEZ', 985678423, 'pato.lopez@gmail.com'),
('009', 'KAROL', 'SEVILLA', 959567832, 'karo.sevilla@gmail.com'),
('010', 'LUCIANA', 'TORRES', 959334422, 'lu.torres@gmail.com'),
('011', 'VALERIA', 'SUAREZ', 903906785, 'vale.sua@gamail.com'),
('012', 'JONATHAN', 'GUTIERREZ', 934656678, 'jona.gutierrez@gmail,com'),
('013', 'ALBERTO', 'MACEDO', 989100004, 'alberto.macedo@gmail.com'),
('014', 'CARLOS', 'FLORES', 909056583, 'carlos.flores@gmail.com'),
('015', 'FERNANDO', 'HUAMAN', 987899123, 'fer.huaman@gmail.com');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sala`
--

CREATE TABLE `sala` (
  `id_sala` varchar(4) NOT NULL,
  `num_sala` int(3) NOT NULL,
  `capa_sala` varchar(3) NOT NULL,
  `id_cine` varchar(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `sala`
--

INSERT INTO `sala` (`id_sala`, `num_sala`, `capa_sala`, `id_cine`) VALUES
('001', 1, '30', '001'),
('002', 2, '35', '002'),
('003', 3, '40', '002'),
('004', 4, '35', '001');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tiket`
--

CREATE TABLE `tiket` (
  `id_tiket` varchar(4) NOT NULL,
  `fecha_tiket` date NOT NULL,
  `hora_tiket` varchar(5) NOT NULL,
  `id_cine` varchar(4) NOT NULL,
  `id_peli` varchar(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `tiket`
--

INSERT INTO `tiket` (`id_tiket`, `fecha_tiket`, `hora_tiket`, `id_cine`, `id_peli`) VALUES
('001', '2004-07-21', '12;50', '001', '005'),
('002', '2009-05-21', '14:50', '002', '002'),
('003', '2010-02-21', '16:30', '001', '003'),
('004', '2003-12-20', '12:15', '001', '005'),
('005', '2020-08-20', '16:30', '001', '006'),
('006', '2019-03-20', '15:15', '001', '003'),
('007', '2012-12-21', '14:50', '002', '004'),
('008', '2016-08-20', '18:20', '002', '005'),
('009', '0000-00-00', '14:20', '001', '003'),
('010', '2012-03-21', '16:30', '002', '004');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `venta`
--

CREATE TABLE `venta` (
  `num_venta` int(4) NOT NULL,
  `fecha_venta` date NOT NULL,
  `dni_cliente` int(8) NOT NULL,
  `id_encargado` varchar(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `venta`
--

INSERT INTO `venta` (`num_venta`, `fecha_venta`, `dni_cliente`, `id_encargado`) VALUES
(1, '2005-05-21', 72253827, '002'),
(2, '2012-02-21', 70736765, '002'),
(4, '2010-03-21', 30764583, '004'),
(5, '0009-03-21', 464457899, '005'),
(6, '2019-04-21', 31325644, '005'),
(7, '2007-04-21', 72253827, '006'),
(8, '2013-04-21', 72253827, '007');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `venta_detalle`
--

CREATE TABLE `venta_detalle` (
  `num_venta` int(4) NOT NULL,
  `id_tiket` varchar(4) NOT NULL,
  `precio_venta` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `venta_detalle`
--

INSERT INTO `venta_detalle` (`num_venta`, `id_tiket`, `precio_venta`) VALUES
(1, '002', 15),
(1, '002', 15);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `butaca`
--
ALTER TABLE `butaca`
  ADD PRIMARY KEY (`cod_but`),
  ADD KEY `id_sala` (`id_sala`);

--
-- Indices de la tabla `cine`
--
ALTER TABLE `cine`
  ADD PRIMARY KEY (`id_cine`);

--
-- Indices de la tabla `cliente`
--
ALTER TABLE `cliente`
  ADD PRIMARY KEY (`dni_cliente`),
  ADD KEY `id_datos` (`id_datos`);

--
-- Indices de la tabla `encargado_vent`
--
ALTER TABLE `encargado_vent`
  ADD PRIMARY KEY (`id_encargado`),
  ADD KEY `id_datos` (`id_datos`);

--
-- Indices de la tabla `pelicula`
--
ALTER TABLE `pelicula`
  ADD PRIMARY KEY (`id_peli`);

--
-- Indices de la tabla `persona`
--
ALTER TABLE `persona`
  ADD PRIMARY KEY (`id_datos`);

--
-- Indices de la tabla `sala`
--
ALTER TABLE `sala`
  ADD PRIMARY KEY (`id_sala`),
  ADD KEY `id_cine` (`id_cine`);

--
-- Indices de la tabla `tiket`
--
ALTER TABLE `tiket`
  ADD PRIMARY KEY (`id_tiket`),
  ADD KEY `id_cine` (`id_cine`),
  ADD KEY `id_peli` (`id_peli`);

--
-- Indices de la tabla `venta`
--
ALTER TABLE `venta`
  ADD PRIMARY KEY (`num_venta`),
  ADD KEY `dni_cliente` (`dni_cliente`),
  ADD KEY `id_encargado` (`id_encargado`);

--
-- Indices de la tabla `venta_detalle`
--
ALTER TABLE `venta_detalle`
  ADD KEY `num_venta` (`num_venta`),
  ADD KEY `id_tiket` (`id_tiket`);

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `butaca`
--
ALTER TABLE `butaca`
  ADD CONSTRAINT `butaca_ibfk_1` FOREIGN KEY (`id_sala`) REFERENCES `sala` (`id_sala`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `cliente`
--
ALTER TABLE `cliente`
  ADD CONSTRAINT `cliente_ibfk_1` FOREIGN KEY (`id_datos`) REFERENCES `persona` (`id_datos`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `encargado_vent`
--
ALTER TABLE `encargado_vent`
  ADD CONSTRAINT `encargado_vent_ibfk_1` FOREIGN KEY (`id_datos`) REFERENCES `persona` (`id_datos`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `sala`
--
ALTER TABLE `sala`
  ADD CONSTRAINT `sala_ibfk_1` FOREIGN KEY (`id_cine`) REFERENCES `cine` (`id_cine`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `tiket`
--
ALTER TABLE `tiket`
  ADD CONSTRAINT `tiket_ibfk_1` FOREIGN KEY (`id_peli`) REFERENCES `pelicula` (`id_peli`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tiket_ibfk_2` FOREIGN KEY (`id_cine`) REFERENCES `cine` (`id_cine`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `venta`
--
ALTER TABLE `venta`
  ADD CONSTRAINT `venta_ibfk_1` FOREIGN KEY (`id_encargado`) REFERENCES `encargado_vent` (`id_encargado`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `venta_ibfk_2` FOREIGN KEY (`dni_cliente`) REFERENCES `cliente` (`dni_cliente`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `venta_detalle`
--
ALTER TABLE `venta_detalle`
  ADD CONSTRAINT `venta_detalle_ibfk_1` FOREIGN KEY (`num_venta`) REFERENCES `venta` (`num_venta`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `venta_detalle_ibfk_2` FOREIGN KEY (`id_tiket`) REFERENCES `tiket` (`id_tiket`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
