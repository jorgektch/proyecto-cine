<?php

//declarando variables para la conexión
$servidor = "localhost"; //el servidor que utilizaremos, en este caso será el localhost
$usuario = "root"; //El usuario de la base de datos
$contrasenha = ""; //La contraseña del usuario que utilizaremos
$BD = "fromulario"; //El nombre de la base de datos

		
// Crea conexión
$conexion = mysqli_connect($servidor, $usuario, $contrasenha, $BD);

// Verifica conexión
if (!$conexion) {
    die("Connection failed: " . mysqli_connect_error());
}

?>
	
	
<html>
<head>
	<title>CLIENTES</title>
	<meta charset="utf-8">
</head>
<body  background="img/fondo.png">

<br><br><center><b><FONT COLOR="#B8860B" FACE="Bookman Old Style" size="5" height="40" width="1000" align=center>LISTA DE CLIENTES  </FONT></br><br></b></center>
<br>
    <center>
	<table border="2" bgcolor="#ECC5C0" > 
		<tr>
			<td align=center width="70">DNI CLIENTE</td>
			<td align=center width="200">NOMBRE </td>
			<td align=center width="60">APELLIDO </td>  
			<td align=center width="90">DIRECCIÓN</td>
			<td align=center width="90">TELÉFONO</td>	
			<td align=center width="200">EMAIL</td>
		</tr>

		<?php
        $consulta= "SELECT * FROM `cliente` ";
        $datos = mysqli_query($conexion, $consulta);

		while($fila=mysqli_fetch_array($datos)){
		 ?>

		<tr>
			<td><?php echo $fila['cod_cli'] ?></td>  
			<td><?php echo $fila['nom_cli'] ?></td>
			<td><?php echo $fila['ape_cli'] ?></td>
			<td><?php echo $fila['direc_cli'] ?></td>
			<td><?php echo $fila['telef_cli'] ?></td>
			<td><?php echo $fila['email_cli'] ?></td>
		</tr>
	<?php 
	}
	?>
	</table>
</center>
<!--
<br><br><br>
	<center><a href="../index.html">REGRESAR</a><h2>
-->
</body>
</html>