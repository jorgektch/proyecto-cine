-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 22-06-2021 a las 02:56:02
-- Versión del servidor: 10.4.14-MariaDB
-- Versión de PHP: 7.4.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `prueba`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cliente`
--

CREATE TABLE `cliente` (
  `cod_cli` varchar(8) NOT NULL,
  `nom_cli` text DEFAULT NULL,
  `ape_cli` text DEFAULT NULL,
  `direc_cli` varchar(20) DEFAULT NULL,
  `telef_cli` varchar(9) DEFAULT NULL,
  `email_cli` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `cliente`
--

INSERT INTO `cliente` (`cod_cli`, `nom_cli`, `ape_cli`, `direc_cli`, `telef_cli`, `email_cli`) VALUES
('11111111', 'Lourdes', 'Zapata', 'Urb. Aurora 222', '212121', 'lzapata@hotmail.com'),
('12345678', 'Juanita', 'Lozada', 'Los Jazmines 47', '545454', 'jlozada@hotmail.com'),
('25252525', 'Mauricio', 'Lazarte Solar', 'Av. Los Incas 123', '363636', 'mlazarte@gmail.com'),
('29292929', 'María', 'Perez', 'Av. Callao123', '242424', 'mperez@hotmail.com'),
('30481277', 'Laura', 'López Gómez', 'Av. La María 123', '212121', 'laurita@gmail.com'),
('555555', 'Mónica', 'Llosa Zegarra', 'Urb. Aurora 123', '444444', 'mllosa@gmail.com'),
('656565', 'Fabiola', 'López Soto', 'Urb. El Diamante 456', '454545', 'flopez@hotmail.com'),
('70610383', 'José', 'Céspedes Alí', 'Av. Los Ángeles 333', '222222', 'ali@gmail.com'),
('74515423', 'Rosario', 'Meneses Sánchez', 'Independencia 444', '232323', 'charito@gmail.com'),
('757575', 'Betty', 'Paz López', 'Urb. Los Angeles 666', '353535', 'bpaz@hotmail.com');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `consulta`
--

CREATE TABLE `consulta` (
  `cod_con` varchar(10) NOT NULL,
  `cod_ma` varchar(8) NOT NULL,
  `cod_ve` varchar(8) NOT NULL,
  `cod_tipo` varchar(3) NOT NULL,
  `fecha_vi` date NOT NULL,
  `tratamiento` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `consulta`
--

INSERT INTO `consulta` (`cod_con`, `cod_ma`, `cod_ve`, `cod_tipo`, `fecha_vi`, `tratamiento`) VALUES
('C0001', 'M0002', '70854596', '2', '2020-02-01', 'Ninguno'),
('C0002', 'M0001', '30124578', '1', '2019-12-11', 'Ninguno'),
('C0003', 'M0001', '70854596', '2', '2020-01-10', 'Ninguno'),
('C0004', 'M0003', '30124578', '5', '2020-01-01', 'Pastillas cólicos'),
('C0006', 'M0004', '30124578', '8', '2019-11-11', 'Post-Operatorio'),
('C0007', 'M0007', '30124578', '1', '2019-12-12', 'Ninguno'),
('C0008', 'M0008', '30124578', '4', '2020-04-01', 'Pastillas cólicos'),
('C0009', 'M0009', '70854596', '3', '2020-06-01', 'Ninguno'),
('C0010', 'M0010', '30124578', '6', '2020-04-01', 'Ninguno'),
('C0011', 'M0011', '30124578', '2', '2020-02-01', 'Especial antipulgas'),
('C0012', 'M0012', '70854596', '5', '2020-06-01', 'Pastillas cólicos'),
('C0013', 'M0013', '70854596', '4', '2020-06-01', 'Ninguno'),
('C0015', 'M0015', '30124578', '7', '2020-04-01', 'Fractura'),
('C0015', 'M0015', '70854596', '1', '2020-06-01', 'Ninguno');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mascotas`
--

CREATE TABLE `mascotas` (
  `cod_ma` varchar(8) NOT NULL,
  `cod_cli` varchar(8) NOT NULL,
  `nom_ma` text NOT NULL,
  `especie_ma` text NOT NULL,
  `raza_ma` text NOT NULL,
  `edad_ma` int(2) NOT NULL,
  `sexo_ma` text NOT NULL,
  `fallecio_ma` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `mascotas`
--

INSERT INTO `mascotas` (`cod_ma`, `cod_cli`, `nom_ma`, `especie_ma`, `raza_ma`, `edad_ma`, `sexo_ma`, `fallecio_ma`) VALUES
('M0001', '30481277', 'BOBY', 'PERRO', 'LABRADOR', 2, 'MACHO', 'NO'),
('M0002', '70610383', 'RABITO', 'GATO', 'BENGALA', 3, 'HEMBRA', 'NO'),
('M0003', '30481277', 'CHISPA', 'GATO', 'SIAMES', 1, 'HEMBRA', 'NO'),
('M0004', '74515423', 'NEGRITO', 'PERRO', 'SHITZU', 8, 'MACHO', 'NO'),
('M0005', '74515423', 'NEGRITO', 'GATO', 'CRIOLLO', 8, 'MACHO', 'NO'),
('M0006', '74515423', 'PULGOSO', 'PERRO', 'DOBERMAN', 8, 'MACHO', 'NO'),
('M0007', '74515423', 'PICAFLOR', 'LORO', 'SELVATICO', 8, 'MACHO', 'NO'),
('M0008', '25252525', 'ESTRELLA', 'GATO', 'BENGALA', 3, 'HEMBRA', 'NO'),
('M0009', '25252525', 'OREJAS', 'CONEJO', 'ANGORA', 1, 'MACHO', 'NO'),
('M0010', '555555', 'TOBY', 'PERRO', 'LABRADOR', 4, 'MACHO', 'NO'),
('M0011', '555555', 'RAMBO', 'PERRO', 'SCHNAUZER', 3, 'MACHO', 'NO'),
('M0012', '656565', 'OSITA', 'PERRO', 'POODLE', 8, 'HEMBRA', 'NO'),
('M0013', '656565', 'LUNA', 'GATO', 'PERSA', 6, 'HEMBRA', 'NO'),
('M0014', '757575', 'THOR', 'PERRO', 'LABRADOR', 10, 'MACHO', 'NO'),
('M0015', '757575', 'MUÑECA', 'AVE', 'GUACAMAYO', 1, 'HEMBRA', 'NO'),
('M0016', '555555', 'OREJAS', 'PERRO', 'BASSET HOUND', 4, 'HEMBRA', 'NO'),
('M0017', '70610383', 'PERLITA', 'gato', 'Criolla', 4, 'HEMBRA', 'NO');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_consulta`
--

CREATE TABLE `tipo_consulta` (
  `cod_tipo` varchar(3) NOT NULL,
  `nom_con` text NOT NULL,
  `precio_con` int(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `tipo_consulta`
--

INSERT INTO `tipo_consulta` (`cod_tipo`, `nom_con`, `precio_con`) VALUES
('1', 'Corte de uñas', 45),
('2', 'Baño', 60),
('3', 'Desparasitación', 50),
('4', 'Castración', 50),
('5', 'Ecografía simple', 50),
('6', 'Ecografía Doppler', 50),
('7', 'Internamiento en clínica (x día)', 120),
('8', 'Internamient en clínica (x hora)', 20);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `veterinario`
--

CREATE TABLE `veterinario` (
  `cod_ve` varchar(8) NOT NULL,
  `nom_ve` text NOT NULL,
  `ape_ve` text NOT NULL,
  `dir_ve` varchar(20) NOT NULL,
  `telefono_ve` int(9) NOT NULL,
  `email_ve` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `veterinario`
--

INSERT INTO `veterinario` (`cod_ve`, `nom_ve`, `ape_ve`, `dir_ve`, `telefono_ve`, `email_ve`) VALUES
('30124578', 'LUCIANA', 'DE LA FUENTE', 'AV. ESTADOS UNIDOS', 985356254, 'LU.DE.LA.FUENTE@GMAI'),
('70854596', 'JOAQUIN', 'RODRIGUEZ', 'AV. ESTADOS UNIDOS', 999999999, 'JRODRIGUEZ@HOTMAIL.C');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `cliente`
--
ALTER TABLE `cliente`
  ADD PRIMARY KEY (`cod_cli`);

--
-- Indices de la tabla `consulta`
--
ALTER TABLE `consulta`
  ADD KEY `cod_ve` (`cod_ve`),
  ADD KEY `cod_ma` (`cod_ma`),
  ADD KEY `cod_tipo` (`cod_tipo`);

--
-- Indices de la tabla `mascotas`
--
ALTER TABLE `mascotas`
  ADD PRIMARY KEY (`cod_ma`),
  ADD KEY `cod_cli` (`cod_cli`);

--
-- Indices de la tabla `tipo_consulta`
--
ALTER TABLE `tipo_consulta`
  ADD PRIMARY KEY (`cod_tipo`);

--
-- Indices de la tabla `veterinario`
--
ALTER TABLE `veterinario`
  ADD PRIMARY KEY (`cod_ve`);

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `consulta`
--
ALTER TABLE `consulta`
  ADD CONSTRAINT `consulta_ibfk_1` FOREIGN KEY (`cod_ve`) REFERENCES `veterinario` (`cod_ve`),
  ADD CONSTRAINT `consulta_ibfk_2` FOREIGN KEY (`cod_ma`) REFERENCES `mascotas` (`cod_ma`),
  ADD CONSTRAINT `consulta_ibfk_3` FOREIGN KEY (`cod_tipo`) REFERENCES `tipo_consulta` (`cod_tipo`);

--
-- Filtros para la tabla `mascotas`
--
ALTER TABLE `mascotas`
  ADD CONSTRAINT `mascotas_ibfk_1` FOREIGN KEY (`cod_cli`) REFERENCES `cliente` (`cod_cli`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
