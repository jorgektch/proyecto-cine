<?php
session_start();
include("conexion.php");
?>
	
	
<html>
<head>
	<title>CLIENTES</title>
	<meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="https://dhbhdrzi4tiry.cloudfront.net/cdn/sites/foundation.min.css">
  </head>
</head>
<body>


<div class="title-bar" data-responsive-toggle="Menu" data-hide-for="small">
  <button class="menu-icon" type="button" data-toggle></button>
  <div class="title-bar-title">Menu</div>
</div>

<div class="top-bar" id="Menu">
  <div class="top-bar-left">
    <ul class="menu" data-responsive-menu="accordion">
      <li class="menu-text">Acceso General</li>
      <li><a href="#">Información</a></li>
      <li><a href="#">Clientes</a></li>
      <li><a href="#">Veterinarios</a></li>
    </ul>
  </div>
  <div class="top-bar-right">
    <ul class="menu">
      <li>Bienvenido...
    </ul>
  </div>
</div>
<!-- /Navegacion -->

<br>

<div class="row">

  <div class="medium-7 large-6 columns">
    <h1>USUARIO</h1>
    <p class="subheader">Esta es la página de prueba de la Semana 14. Validaremos el usuario con la contraseña ingresados mediante formulario a través de un archivo php llamado VALIDAR que contrastará lo ingresado con la tabla que contiene el usuario y la contraseña.</p>
    <a href="mostrarveterinario.php"><button class="button" >Mostrar Veterinarios</button></a>
    <a href="mostrarclientes.php"><button class="button" >Mostrar Clientes</button></a>
    <a href="ingresarcliente.php"><button class="button" >Agregar Clientes</button></a>
  </div>

  <div class="show-for-large large-3 columns">
    <img src="img/candado.png" alt="validando usuario">
  </div>

  <div class="medium-5 large-3 columns">
    <div class="callout secondary">
      <form action="cerrarsesion.php" method="POST">
        <div class="row">
          <div class="small-12 columns">
       <!--     <label>Ingrese su usuario
              <input type="text" placeholder="5letras" name="usuario">
            </label>
          </div>
          <div class="small-12 columns">
            <label>Ingrese su contraseña
              <input type="number" placeholder="4números" name="clave">
            </label>
            <button type="submit" class="button" disabled="">Ingresar</button>-->
			<button type="submit" class="button">Cerrar Sesión</button>
          </div>
        </div>
      </form>
    </div>
  </div>

</div>

<div class="row column">
  <hr>
</div>

<center>
<h1> DATOS PERSONALES </h1>
    <form action="grabarcliente.php" method="POST"> 
		DNI:   <input type="texto" placeholder="Ingrese su DNI" name="DNI"> <br><br>
		Nombre:   <input type="texto" placeholder="Ingrese su nombre" name="nombre"> <br><br>
		Apellido: <input type="texto" placeholder="Ingrese su apellido"name="apellido"> <br><br>
		Dirección: <input type="texto" placeholder="Ingrese su dirección" name="direccion"> <br><br>
		Teléfono: <input type="texto" placeholder="Ej. (54)242424" name="telefono"> <br><br>
		EMAIL: <input type="texto" name="email"> <br><br>
		<input type="submit" name="enviar" value="Enviar">
		<input type="reset" name="borrar" value="Borrar">	
   </form>
</center>   
</body>
</html>
