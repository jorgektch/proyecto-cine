<?php
	session_start();
?>

<!doctype html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>SEMANA 14</title>
    <link rel="stylesheet" href="https://dhbhdrzi4tiry.cloudfront.net/cdn/sites/foundation.min.css">
  </head>
  <body>

<!-- Navegación-->
<div class="title-bar" data-responsive-toggle="Menu" data-hide-for="small">
  <button class="menu-icon" type="button" data-toggle></button>
  <div class="title-bar-title">Menu</div>
</div>

<div class="top-bar" id="Menu">
  <div class="top-bar-left">
    <ul class="menu" data-responsive-menu="accordion">
      <li class="menu-text">Acceso General</li>
      <li><a href="#">Información</a></li>
      <li><a href="#">Clientes</a></li>
      <li><a href="#">Veterinarios</a></li>
    </ul>
  </div>
  <div class="top-bar-right">
    <ul class="menu">
      <li>Bienvenido ...</li>
     </ul>
  </div>
</div>
<!-- /Navegacion -->

<br>

<div class="row">

  <div class="medium-7 large-6 columns">
    <h1>USUARIO</h1>
    <p class="subheader">Esta es la página de prueba de la Semana 14. Validaremos el usuario con la contraseña ingresados mediante formulario a través de un archivo php llamado VALIDAR que contrastará lo ingresado con la tabla que contiene el usuario y la contraseña.</p>
    <a href="mostrarveterinario.php"><button class="button" >Mostrar Veterinarios</button></a>
    <a href="mostrarclientes.php"><button class="button" >Mostrar Clientes</button></a>
    <a href="ingresarcliente.php"><button class="button" >Agregar Clientes</button></a>
  </div>

  <div class="show-for-large large-3 columns">
    <img src="img/candado.png" alt="validando usuario">
  </div>

  <div class="medium-5 large-3 columns">
    <div class="callout secondary">
      <form action="cerrarsesion.php" method="POST">
        <div class="row">
          <div class="small-12 columns">
          <!--  <label>Ingrese su usuario
              <input type="text" placeholder="5letras" name="usuario">
            </label>
          </div>
          <div class="small-12 columns">
            <label>Ingrese su contraseña
              <input type="number" placeholder="4números" name="usuario">
            </label>
            <!--<button type="submit" class="button">Ingresar</button> -->
			<button type="submit" class="button">Cerrar Sesión</button>
          </div>
        </div>
      </form>
    </div>
  </div>  

</div>

<div class="row column">
  <hr>
</div>

<div class="row column">
  <p class="lead">Trending Planetary Destinations</p>
</div>

<div class="row small-up-1 medium-up-2 large-up-3">
  <div class="column">
    <div class="callout">
      <p>Pegasi B</p>
      <p><img src="https://placehold.it/400x370&text=Pegasi B" alt="image of a planet called Pegasi B"></p>
      <p class="lead">Copernican Revolution caused an uproar</p>
      <p class="subheader">Find Earth-like planets life outside the Solar System</p>
    </div>
  </div>
  <div class="column">
    <div class="callout">
      <p>Pegasi B</p>
      <p><img src="https://placehold.it/400x370&text=Pegasi B" alt="image of a planet called Pegasi B"></p>
      <p class="lead">Copernican Revolution caused an uproar</p>
      <p class="subheader">Find Earth-like planets life outside the Solar System</p>
    </div>
  </div>
  <div class="column">
    <div class="callout">
      <p>Pegasi B</p>
      <p><img src="https://placehold.it/400x370&text=Pegasi B" alt="image of a planet called Pegasi B"></p>
      <p class="lead">Copernican Revolution caused an uproar</p>
      <p class="subheader">Find Earth-like planets life outside the Solar System</p>
    </div>
  </div>
  <div class="column">
    <div class="callout">
      <p>Pegasi B</p>
      <p><img src="https://placehold.it/400x370&text=Pegasi B" alt="image of a planet called Pegasi B"></p>
      <p class="lead">Copernican Revolution caused an uproar</p>
      <p class="subheader">Find Earth-like planets life outside the Solar System</p>
    </div>
  </div>
  <div class="column">
    <div class="callout">
      <p>Pegasi B</p>
      <p><img src="https://placehold.it/400x370&text=Pegasi B" alt="image of a planet called Pegasi B"></p>
      <p class="lead">Copernican Revolution caused an uproar</p>
      <p class="subheader">Find Earth-like planets life outside the Solar System</p>
    </div>
  </div>
  <div class="column">
    <div class="callout">
      <p>Pegasi B</p>
      <p><img src="https://placehold.it/400x370&text=Pegasi B" alt="image of a planet called Pegasi B"></p>
      <p class="lead">Copernican Revolution caused an uproar</p>
      <p class="subheader">Find Earth-like planets life outside the Solar System</p>
    </div>
  </div>

</div>

<div class="row column">
  <a class="button hollow expanded">Load More</a>
</div>

<footer>
  

</footer>

    <script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
    <script src="https://dhbhdrzi4tiry.cloudfront.net/cdn/sites/foundation.js"></script>
    <script>
      $(document).foundation();
    </script>
  </body>
</html>
