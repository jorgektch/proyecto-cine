<?php
	//session_start();
?>
<!doctype html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>SESIONES</title>
    <link rel="stylesheet" href="https://dhbhdrzi4tiry.cloudfront.net/cdn/sites/foundation.min.css">
  </head>
  <body>

<!-- Navegación-->
<div class="title-bar" data-responsive-toggle="realEstateMenu" data-hide-for="small">
  <button class="menu-icon" type="button" data-toggle></button>
  <div class="title-bar-title">Menu</div>
</div>

<div class="top-bar" id="Menu">
  <div class="top-bar-left">
    <ul class="menu" data-responsive-menu="accordion">
      <li class="menu-text">Acceso General</li>
      <li><a href="#">Información</a></li>
      <li><a href="#">Clientes</a></li>
      <li><a href="#">Veterinarios</a></li>
    </ul>
  </div>
  <div class="top-bar-right">
    <ul class="menu">
      <li><a href="#">Mi cuenta</a></li>
      <li><a class="button">Login</a></li>
	  
    </ul>
  </div>
</div>
<!-- /Navegacion -->

<br>

<div class="row">

  <div class="medium-7 large-6 columns">
    <h1>PAGINA DE PRUEBA LOGUEANDO USUARIO</h1>
    <p class="subheader">Esta es la página de prueba de la Semana 13. 
	Validaremos el usuario con la contraseña ingresados mediante formulario a través de un archivo
	php llamado VALIDAR que contrastará lo ingresado con la tabla que contiene el usuario y la contraseña.</p>
    <a href="mostrarveterinario.php"><button class="button" disabled="">Mostrar Veterinarios</button></a>
   	<a href="mostrarclientes.php"><button class="button" disabled="">Mostrar Clientes</button></a>
    <a href="ingresarcliente.php"><button class="button" disabled="">Agregar Clientes</button></a>
  </div>

  <div class="show-for-large large-3 columns">
    <img src="img/candado.png" alt="validando usuario">
  </div>

  <div class="medium-5 large-3 columns">
    <div class="callout secondary">
      <form action="validarsesion.php" method="POST">
        <div class="row">
          <div class="small-12 columns">
            <label>Ingrese su usuario
              <input type="text" placeholder="Ingrese Usuario" name="usuario">
            </label>
          </div>
          <div class="small-12 columns">
            <label>Ingrese su contraseña
              <input type="number" placeholder="1234" name="clave">
            </label>
            <center><button type="submit" class="button">Ingresar</button>
			<!-- <button type="submit" class="button">Cerrar </button></center> -->
		   </div>
        </div> 
      </form>
    </div>
  </div>

</div>

<div class="row column">
  <hr>
</div>
    <script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
    <script src="https://dhbhdrzi4tiry.cloudfront.net/cdn/sites/foundation.js"></script>
    <script>
      $(document).foundation();
    </script>
  </body>
</html>
