<?php
session_start();
include("conexion.php");

?>
	
	
<html>
<head>
	<title>VETERINARIO</title>
	<meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="https://dhbhdrzi4tiry.cloudfront.net/cdn/sites/foundation.min.css">
  </head>
</head>
<body>


<div class="title-bar" data-responsive-toggle="realEstateMenu" data-hide-for="small">
  <button class="menu-icon" type="button" data-toggle></button>
  <div class="title-bar-title">Menu</div>
</div>

<div class="top-bar" id="Menu">
  <div class="top-bar-left">
    <ul class="menu" data-responsive-menu="accordion">
      <li class="menu-text">Acceso General</li>
      <li><a href="#">Información</a></li>
      <li><a href="#">Clientes</a></li>
      <li><a href="#">Veterinarios</a></li>
    </ul>
  </div>
  <div class="top-bar-right">
    <ul class="menu">
      <li>Bienvenido...
    </ul>
  </div>
</div>
<!-- /Navegacion -->

<br>

<div class="row">

  <div class="medium-7 large-6 columns">
    <h1>USUARIO</h1>
    <p class="subheader">Esta es la página de prueba de la Semana 13. Validaremos el usuario con la contraseña ingresados mediante formulario a través de un archivo php llamado VALIDAR que contrastará lo ingresado con la tabla que contiene el usuario y la contraseña.</p>
    <a href="mostrarveterinario.php"><button class="button" >Mostrar Veterinarios</button></a>
    <a href="mostrarclientes.php"><button class="button" >Mostrar Clientes</button></a>
    <a href="ingresarcliente.php"><button class="button" >Agregar Clientes</button></a>
  </div>

  <div class="show-for-large large-3 columns">
    <img src="img/candado.png" alt="validando usuario">
  </div>

  <div class="medium-5 large-3 columns">
    <div class="callout secondary">
      <form action="cerrarsesion.php" method="POST">
        <div class="row">
          <div class="small-12 columns">
           <!-- <label>Ingrese su usuario
              <input type="text" placeholder="5letras" name="usuario">
            </label>
          </div>
          <div class="small-12 columns">
            <label>Ingrese su contraseña
              <input type="number" placeholder="4números" name="clave">
            </label>
            <button type="submit" class="button">Ingresar</button> -->
			<button type="submit" class="button">Cerrar Sesión</button>
          </div>
        </div>
      </form>
    </div>
  </div>

</div>

<div class="row column">
  <hr>
</div>




<center><b><i><FONT COLOR="black" FACE="Bookman Old Style" size="6" height="40" width="1000" align=center>TABLA DE VETERINARIOS  </FONT></br><br></b></i></center>
<br>
    <center>
	<table border="1" > 
		<!--generamos la tabla -->
		<tr>
			<!-- nombres de los campos de la tabla -->
			<td>CODIGO</td>
			<td>NOMBRE</td>
			<td>APELLIDO</td>  
			<td>DIRECCION</td>
			<td>TELEFONO</td>	
		</tr>

		<?php
        $consulta= "SELECT cod_ve, nom_ve, ape_ve, dir_ve, telefono_ve FROM `veterinario` ";
        $datos = mysqli_query($conexion, $consulta);

		while($fila=mysqli_fetch_array($datos)){
		 ?>

		<tr>
			<td><?php echo $fila['cod_ve'] ?></td>  
			<td><?php echo $fila['nom_ve'] ?></td>
			<td><?php echo $fila['ape_ve'] ?></td>
			<td><?php echo $fila['dir_ve'] ?></td>
			<td><?php echo $fila['telefono_ve'] ?></td>
			<td><a href="eliminar.php?cod=<?php echo $fila['cod_ve'] ?>">ELIMINAR</a></td>	
			<td><a href="actualizar.php?cod=<?php echo $fila['cod_ve'] ?>">ACTUALIZAR</a></td>	
		</tr>
	<?php 
	}
	 ?>
	</table>
	
	</center>
</body>
</html>