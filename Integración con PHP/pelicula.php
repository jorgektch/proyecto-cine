<?php
include('conexion.php');
session_start();
?>
<html>
    <head>
        <title> CINEPLANET</title>
        <meta charset="UTF-8"/>
		<link href="css/estilom.css" rel ="stylesheet">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    </head>
  
    <body align="center">
	
        <header>
            <div id="logo">
	            <img src="img/logo.jpg" alt="logo">
	               
	        </div>
			
	        <div id="logo1">
	            <img src="img/logo1.jpg" alt="teléfono">
	               <strong><p> 054(458967) - 936530307<p></strong>
	        </div>
        </header>
		
		<section id="info">
            <nav class="menu">
	            <ul>
<?php
	
	if(!isset($_SESSION["correo_cliente"])){
		echo "
		            <li><a href='index.php'>INICIO</a></li>
		            <li><a href='conocenos.php'>CONÓCENOS</a></li>
		            <li><a href='registro.php'>REGISTRO</a></li>
		            <li><a href='ingreso.php'>INGRESO</a></li>
		";
	}else{
		echo "
		            <li><a href='index.php'>INICIO</a></li>
		            <li><a href='ciudad.php'>CARTELERA</a></li>
		            <li><a href='conocenos.php'>CONÓCENOS</a></li>
		            <li><a href='sesion_cerrar.php'>CERRAR SESIÓN</a></li>

		";
	}
?>
	            </ul>
            </nav>
  
	
		<br>

		

<?php
	if(!isset($_SESSION["correo_cliente"])){
		echo "
			<h3>Debes iniciar sesión para ver la cartelera</h3>
			<p class='link'>Click aquí para <a href='ingreso.php'>Iniciar sesión</a>.</p>
		";
	}else{
?>
		<div class="galeria">
	
			
			<div class="title-img">
			<?php
				
			
				$query = "SELECT * FROM `genero` WHERE id_genero = '{$_SESSION['id_genero_global']}'";
				$result = mysqli_query($con, $query);
				if ($result->num_rows > 0) {
					while ($row_genero = $result->fetch_assoc()) {
						echo "
				<h2>GENERO: {$row_genero['nombre_genero']}</h2>
						";
					}
				}else{
					echo '<h2>GENERO: Género no registrado';
				}
				
			?>
			</div>

			<?php
				$query = "SELECT * FROM `pelicula` WHERE id_genero = '{$_SESSION['id_genero_global']}'";
				$result = mysqli_query($con, $query);
				if ($result->num_rows > 0) {
					while ($row_pelicula = $result->fetch_assoc()) {
						echo "
			<div class='box-img acc'>
			    <a href='reserva.php' id='{$row_pelicula['id_pelicula']}' class='pelicula'>
				<img src='imag/{$row_pelicula['imagen_pelicula']}' alt='acc1'>
			</div>
						";
					}
				}else{
					echo '<p>No hay peliculas disponibles</p>';
				}
				
			?>
			
			
	    </div>

<?php
		
	}
?>

	    </section>
		
		
     <!--tercera parte-->
        
		<div id="redes">
	         
			<ul>
	            <li> <img src="img/face.png" alt="aspa"><a href="https://www.facebook.com/cinemarkperu/" >FACEBOOK</a></li>
	            <li> <img src="img/twitter.jpg" alt="aspa"><a href="https://www.instagram.com/cinemarkperu/?hl=en" >INSTAGRAM</a></li>
		        <li> <img src="img/instagram.png" alt="aspa"><a href="https://twitter.com/cinemark_peru?lang=en" >TWITTER</a></li>
	        </ul>
        
        </div>



		<script type="text/javascript">
		$(document).ready(function(){	
			$('.pelicula').on("click", function(){
				var id_pelicula = $(this).attr('id');
				$.ajax({
                    url: "pelicula_script.php",
                    type:"POST",
					cache:false,
                    data:{id_pelicula:id_pelicula},
                    success:function() {
                    }
                });
            });
        });
        </script>
    </body>
 </html>