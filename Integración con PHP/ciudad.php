<?php
include('conexion.php');
session_start();
$_SESSION["id_cine_global"] = -1;
$_SESSION["id_genero_global"] = -1;
$_SESSION['id_pelicula_global'] = -1;
$_SESSION['id_funcion_global'] = -1;
$_SESSION['id_sala_global'] = -1;
$_SESSION['id_butaca_global'] = -1;
$_SESSION['id_venta_global'] = -1;
$_SESSION['id_monto_global'] = 20;
?>
<html>
	<head>
		<title> Ciudad </title>
		 <meta charset="UTF-8"/>
        <!--<link href="css/diseño.css" rel ="stylesheet">-->
        <link href="css/estilom.css" rel ="stylesheet">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
	</head>
	<body align="center">
	
        <header>
            <div id="logo">
	            <img src="img/logo.jpg" alt="logo">
	               
	        </div>
			
	        <div id="logo1">
	            <img src="img/logo1.jpg" alt="teléfono">
	               <strong><p> 054(458967) - 936530307<p></strong>
	        </div>
        </header>
		
		<section id="info">
            <nav class="menu">

	            <ul>

<?php
	
	if(!isset($_SESSION["correo_cliente"])){
		echo "
		            <li><a href='index.php'>INICIO</a></li>
		            <li><a href='conocenos.php'>CONÓCENOS</a></li>
		            <li><a href='registro.php'>REGISTRO</a></li>
		            <li><a href='ingreso.php'>INGRESO</a></li>
		";
	}else{
		echo "
		            <li><a href='index.php'>INICIO</a></li>
		            <li><a href='ciudad.php'>CARTELERA</a></li>
		            <li><a href='conocenos.php'>CONÓCENOS</a></li>
		            <li><a href='sesion_cerrar.php'>CERRAR SESIÓN</a></li>

		";
	}
?>
	            </ul>
            </nav>



		<div id="contenedor">
<?php
	
	if(!isset($_SESSION["correo_cliente"])){
		echo "
			<h3>Debes iniciar sesión para ver la cartelera</h3>
			<p class='link'>Click aquí para <a href='ingreso.php'>Iniciar sesión</a>.</p>
		";
	}else{
?>
        	<h1>Selecciona el cine</h1>

			<form class="formulario" name ="form1" method="POST" action="pelicula-categoria.php">
				
				
				<label class="label">Ciudad</label><br>
				<select class="select" id="ciudad" name="ciudad" size= "1">
					<option value="">Selecciona una ciudad</option>
					<?php
						$query = "SELECT * FROM `ciudad`";
						$result = mysqli_query($con, $query);
						if ($result->num_rows > 0) {
							while ($row_ciudad = $result->fetch_assoc()) {
								
								echo '<option value="'.$row_ciudad['id_ciudad'].'">'.$row_ciudad['nombre_ciudad'].'</option>';
								
							}
						}else{
							echo '<option value="">No hay ciudades disponibles</option>';
						}
						/*
						<option value="AREQUIPA">AREQUIPA </option>
						<option value="PIURA">PIURA </option>
						<option value="LIMA">LIMA </option>
						<option value="CUSCO">CUSCO </option>
						*/
					?>

				</select><br>
				<label class="label">Cine</label><br>
				<select class="select" id="cine" name="cine" size= "1">
					<option value="">Selecciona un cine</option>
					<!--	
					<option value="Cineplanet Mall Plaza">Cineplanet Mall Plaza </option>
					<option value="Cineplanet Centro Civico">Cineplanet Centro Civico </option>
					<option value="Cineplanet Piura Real Plaza">Cineplanet Piura Real Plaza </option>
					<option value="Cineplanet Cusco">Cineplanet Cusco </option>
					-->
					
				</select></p></h2>
				
				<input class="button" type="submit" name="enviar" value="Continuar">
				<!--<a class="button" href="pelicula-categoria.php">Continuar</a>-->
			</form>
<?php
		
	}
?>
		</div>


    	<!--tercera parte-->
		<div id="redes">
			<ul>
	            <li> <img src="img/face.png" alt="aspa"><a href="https://www.facebook.com/cinemarkperu/">Facebook</a></li>
	            <li> <img src="img/twitter.jpg" alt="aspa"><a href="https://www.instagram.com/cinemarkperu/?hl=en" >Twitter</a></li>
		        <li> <img src="img/instagram.png" alt="aspa"><a href="https://twitter.com/cinemark_peru?lang=en" >Instagram</a></li>
	        </ul>
        </div>

        <script type="text/javascript">
		$(document).ready(function(){
			// ciudad (Dependencia Ajax)
			$("#ciudad").on("change",function(){	
				var id_ciudad = $(this).val();
				$.ajax({
					url :"ciudad_script.php",
					type:"POST",
					cache:false,
					data:{id_ciudad:id_ciudad},
					success:function(data){
						$("#cine").html(data);
					}
				});
			});
			$("#cine").on("change", function(){
				// cine (Dependencia Ajax)
				var id_cine = $(this).val();
				$.ajax({
					url :"ciudad_script.php",
					type:"POST",
					cache:false,
					data:{id_cine:id_cine},
					success:function(){
					}
				});
			});
		});
		</script>
	</body>
	
</html>