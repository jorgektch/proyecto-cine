<?php
include('conexion.php');
?>
<html lang="es">
    <head>
        <title>CINEPLANET</title>
        <meta charset="UTF-8"/>
        <link href="css/estilom.css" rel ="stylesheet">
    </head>
  
    <body align="center">
  		<!--primera parte-->
        <header>
            <div id="logo">
	            <img src="img/logo.jpg" alt="logo">
	               
	        </div>
			
	        <div id="logo1">
	            <img src="img/logo1.jpg" alt="teléfono">
	               <strong><p> 054(458967) - 936530307<p></strong>
	        </div>
        </header>
    	<!--segunda parte-->
        <section id="info">
            <nav class="menu">
	            <ul>
<?php
	
	if(!isset($_SESSION["correo_cliente"])){
		echo "
		            <li><a href='index.php'>INICIO</a></li>
		            <li><a href='conocenos.php'>CONÓCENOS</a></li>
		            <li><a href='registro.php'>REGISTRO</a></li>
		            <li><a href='ingreso.php'>INGRESO</a></li>
		";
	}else{
		echo "
		            <li><a href='index.php'>INICIO</a></li>
		            <li><a href='ciudad.php'>CARTELERA</a></li>
		            <li><a href='conocenos.php'>CONÓCENOS</a></li>
		            <li><a href='sesion_cerrar.php'>CERRAR SESIÓN</a></li>

		";
	}
?>

	            </ul>
            </nav>
            <div id="contenedor">
            	<h1>Registro de usuario</h1>

<?php

	require('conexion.php');
	if (isset($_REQUEST['correo_cliente'])) {
		$dni_cliente = mysqli_real_escape_string($con, $_REQUEST['dni_cliente']);
		$nombre_cliente = mysqli_real_escape_string($con, $_REQUEST['nombre_cliente']);
		$apellido_cliente = mysqli_real_escape_string($con, $_REQUEST['apellido_cliente']);
		$telefono_cliente = mysqli_real_escape_string($con, $_REQUEST['telefono_cliente']);
		$correo_cliente = mysqli_real_escape_string($con, $_REQUEST['correo_cliente']);
		$clave_cliente = mysqli_real_escape_string($con, $_REQUEST['clave_cliente']);

		$query = "INSERT into `cliente` (
			dni_cliente,
			nombre_cliente,
			apellido_cliente,
			telefono_cliente,
			correo_cliente,
			clave_cliente
			)
			VALUES (
			'$dni_cliente',
			'$nombre_cliente',
			'$apellido_cliente',
			'$telefono_cliente',
			'$correo_cliente',
			'$clave_cliente'

		)";

		$result = mysqli_query($con, $query);
		if($result){
			echo "
				<h3>¡Registro exitoso!</h3>
				<p class='link'>Click aquí para iniciar sesión <a href='ingreso.php'>Ingreso</a> nuevamente.</p>
			";
		}else{
			echo "
				<h3>Hay campos requeridos vacíos.</h3>
				<p class='link'>Click aquí para ir al <a href='registro.php'>Registro</a> nuevamente.</p>
			";
		}
	}else{
?>







<?php
	
	if(!isset($_SESSION["correo_cliente"])){


?>


	<form class="formulario" action="" method="post">
		<label class="label">Número de DNI</label>
		<input class="input" type="text" name="dni_cliente" id="dni_cliente" required><br>
		
		<label class="label">Nombres</label>
		<input class="input" type="text" name="nombre_cliente" id="nombre_cliente"><br>

		<label class="label">Apellidos</label>
		<input class="input" type="text" name="apellido_cliente" id="apellido_cliente"><br>

		<label class="label">Teléfono del cliente</label>
		<input class="input" type="text" name="telefono_cliente" id="telefono_cliente"><br>

		<label class="label">Correo electrónico (usuario)</label>
		<input class="input" type="text" name="correo_cliente" id="correo_cliente" required><br>
	    
	    <label class="label">Contraseña</label>
		<input class="input" type="password" name="clave_cliente" id="clave_cliente" required><br>

		<input class="button" type="submit" name="enviar" value="Registrar cuenta">
		
	</form>
			<p>¿Ya tienes una cuenta de usuario? Inicia sesión desde <a href="ingreso.php">aquí</a></p>

<?php
	}else{
		echo "
	<section id='formulario1'>
			<h3>Ya has inicado sesión.</h3>
			<p class='link'>Click aquí para <a href='sesion_cerrar.php'>Cerrar sesión</a>.</p>
	</section>
		";
	}
?>






<?php

	}

?>
	

			</div>
	    </section>
		
    	<!--tercera parte-->
		<div id="redes">
			<ul>
	            <li> <img src="img/face.png" alt="aspa"><a href="https://www.facebook.com/cinemarkperu/">Facebook</a></li>
	            <li> <img src="img/twitter.jpg" alt="aspa"><a href="https://www.instagram.com/cinemarkperu/?hl=en" >Twitter</a></li>
		        <li> <img src="img/instagram.png" alt="aspa"><a href="https://twitter.com/cinemark_peru?lang=en" >Instagram</a></li>
	        </ul>
        </div>
    </body>
 </html>