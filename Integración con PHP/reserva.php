<?php
include('conexion.php');
session_start();
?>
 <html>
    <head>
        <title> CINEPLANET </title>
        <meta charset="UTF-8"/>
        <link href="css/estilo5.css" rel ="stylesheet">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    </head>
    <body align="center">
  <!--primera parte-->
        <header>
            <div id="logo">
	            <img src="img/logo.jpg" alt="logo">
	               
	        </div>
			
	        <div id="logo1">
	            <img src="img/logo1.jpg" alt="teléfono">
	               <strong><p> 054(458967) - 936530307<p></strong>
	        </div>
        </header>
    <!--segunda parte-->
            <nav class="menu">
	            <ul>
<?php
	
	if(!isset($_SESSION["correo_cliente"])){
		echo "
		            <li><a href='index.php'>INICIO</a></li>
		            <li><a href='conocenos.php'>CONÓCENOS</a></li>
		            <li><a href='registro.php'>REGISTRO</a></li>
		            <li><a href='ingreso.php'>INGRESO</a></li>
		";
	}else{
		echo "
		            <li><a href='index.php'>INICIO</a></li>
		            <li><a href='ciudad.php'>CARTELERA</a></li>
		            <li><a href='conocenos.php'>CONÓCENOS</a></li>
		            <li><a href='sesion_cerrar.php'>CERRAR SESIÓN</a></li>

		";
	}
?>

	            </ul>
            </nav>
			
			<table id="tabla">
			    <caption>
			        <nav class="menu2">
	                    <ul>
		                <li> <a href="datos.php" >Datos</a></li>
		                <li> <a href="reserva.php" >Reserva</a></li>
		                <li> <a href="compra.php" >Compra</a></li> 
	                    </ul>
                    </nav>
			    </caption>
			</table>


			<?php
				
			
				$query = "SELECT * FROM `pelicula` WHERE id_pelicula = '{$_SESSION['id_pelicula_global']}'";
				$result = mysqli_query($con, $query);
				if ($result->num_rows > 0) {
					while ($row_pelicula = $result->fetch_assoc()) {
						echo "
				<h2>Película: {$row_pelicula['nombre_pelicula']}</h2>
						";
					}
				}else{
					echo '<h2>Película: Película no registrada</h2>';
				}
			?>

			       
				   <form class="formulario" action="butaca.php">

				   		<label class="label">Función</label><br>
						<select class="select form-control" id="funcion" name="funcion" size= "1">
							<option value="">Selecciona una función</option>
							<?php
								$query = "SELECT * FROM `funcion`
										  WHERE id_pelicula = '{$_SESSION['id_pelicula_global']}' AND id_cine = '{$_SESSION['id_cine_global']}'";
								$result = mysqli_query($con, $query);
								if ($result->num_rows > 0) {
									while ($row_funcion = $result->fetch_assoc()) {
										
										echo '<option value="'.$row_funcion['id_funcion'].'">'.$row_funcion['nombre_funcion'].'</option>';
										$_SESSION["id_sala_global"] = $row_funcion['id_sala'];
									}
								}else{
									echo '<option value="">No hay funciones disponibles</option>';
								}
								
							?>

						</select>
						<input class="button" type="submit" name="enviar" value="Continuar">
						<!--<a href="butaca.php">Continuar</a>-->
				    
			        </form>

        <script type="text/javascript">
		$(document).ready(function(){
			$("#funcion").on("change", function(){
				// funcion (Dependencia Ajax)
				var id_funcion = $(this).val();
				$.ajax({
					url :"reserva_script.php",
					type:"POST",
					cache:false,
					data:{id_funcion:id_funcion},
					success:function(){
					}
				});
			});
		});
		</script>
    </body>
			
</html>